﻿namespace ProyekSDP
{
    partial class AdminPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(104, 130);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(140, 41);
            this.panel1.TabIndex = 7;
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
            this.panel1.MouseHover += new System.EventHandler(this.panel1_MouseHover);
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(104, 177);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(140, 41);
            this.panel2.TabIndex = 8;
            this.panel2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel2_MouseClick);
            this.panel2.MouseHover += new System.EventHandler(this.panel2_MouseHover);
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(104, 224);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(140, 41);
            this.panel3.TabIndex = 8;
            this.panel3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel3_MouseClick);
            this.panel3.MouseHover += new System.EventHandler(this.panel3_MouseHover);
            // 
            // panel4
            // 
            this.panel4.Location = new System.Drawing.Point(104, 271);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(140, 41);
            this.panel4.TabIndex = 8;
            this.panel4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel4_MouseClick);
            this.panel4.MouseHover += new System.EventHandler(this.panel4_MouseHover);
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(104, 318);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(140, 41);
            this.panel5.TabIndex = 8;
            this.panel5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel5_MouseClick);
            this.panel5.MouseHover += new System.EventHandler(this.panel5_MouseHover);
            // 
            // panel6
            // 
            this.panel6.Location = new System.Drawing.Point(566, 130);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(49, 41);
            this.panel6.TabIndex = 9;
            this.panel6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel6_MouseClick);
            this.panel6.MouseHover += new System.EventHandler(this.panel6_MouseHover);
            // 
            // panel7
            // 
            this.panel7.Location = new System.Drawing.Point(630, 130);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(53, 41);
            this.panel7.TabIndex = 9;
            this.panel7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel7_MouseClick);
            this.panel7.MouseHover += new System.EventHandler(this.panel7_MouseHover);
            // 
            // AdminPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "AdminPage";
            this.Text = "AdminPage";
            this.Load += new System.EventHandler(this.AdminPage_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.AdminPage_Paint);
            this.MouseHover += new System.EventHandler(this.AdminPage_MouseHover);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
    }
}